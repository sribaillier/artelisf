<?php

namespace ArteliBundle\Controller;

use ArteliBundle\Entity\Agenda;
use ArteliBundle\Entity\Fresque;
use ArteliBundle\Entity\Grr;
use ArteliBundle\Entity\GrrExclude;
use ArteliBundle\Entity\Newsletter;
use ArteliBundle\Form\ContactType;
use ArteliBundle\Form\FresqueType;
use ArteliBundle\Form\NewsletterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use ArteliBundle\Controller\AgendaController;
class ArteliController extends Controller
{
    /**
     * @Route("/", name="arteli_home")
     */
    public function indexAction()
    {
        $event = $this->getDoctrine()->getManager()->getRepository('ArteliBundle:Agenda')->getEvent();
        $email = new Newsletter();
        $formNewsletter = $this->createForm(NewsletterType::class, $email,array('action' => $this->generateUrl('arteli_email_subscribe')));

        $mot = new Fresque();
        $formFresqueurbaine = $this->createForm(FresqueType::class,$mot,array('action' => $this->generateUrl('arteli_send_word_fresque')));
        return $this->render('ArteliBundle::presentation.html.twig',array('events' => $event, 'formNewsletter' => $formNewsletter->createView(),'formFresque' => $formFresqueurbaine->createView()));

    }

    /**
     * @Route("/{switch}",name="arteli_switch")
     */
    public function switchAction($switch)
    {
        $render = array();

        $render['events'] = $this->getDoctrine()->getManager()->getRepository('ArteliBundle:Agenda')->getEvent();

        $email = new Newsletter();
        $form = $this->createForm(NewsletterType::class, $email,array('action' => $this->generateUrl('arteli_email_subscribe')));
        $render['form'] = $form->createView();
        $formNewsletter = $this->createForm(NewsletterType::class, $email,array('action' => $this->generateUrl('arteli_email_subscribe')));
        $formFresqueurbaine = $this->createForm(FresqueType::class,null,array('action' => $this->generateUrl('arteli_send_word_fresque')));
        $render['formFresque'] = $formFresqueurbaine->createView();
        $render['formNewsletter'] =$formNewsletter->createView();
        switch($switch) {
            case 'presentation':
                return $this->render('ArteliBundle::presentation.html.twig',$render);
                break;
            case 'projet':
                return $this->render('ArteliBundle::projet.html.twig',$render);
                break;
            case 'projetAllMini':
                return $this->render('ArteliBundle::projetAllMini.html.twig',$render);
                break;
            case 'catalogue':
                return $this->render('@Arteli/realisation_catalogue_guide.html.twig',$render);
            case 'fresquepapier':
                return $this->render('@Arteli/realisation_fresque_papier.html.twig',$render);
                break;
            case 'fresquemural':
                return $this->render('@Arteli/realisation_fresque_mural.html.twig',$render);
                break;
            case 'kamishibai':
                return $this->render('ArteliBundle::realisation_kamishibai.html.twig',$render);
                break;
            case 'mosaique':
                return $this->render('ArteliBundle::realisation_mosaique.html.twig',$render);
                break;
            case 'portrait':
                return $this->render('ArteliBundle::realisation_portrait.html.twig',$render);
                break;
            case 'volume':
                return $this->render('ArteliBundle::realisation_volume.html.twig',$render);
                break;
            case 'renovation':
                return $this->render('ArteliBundle::realisation_renovation.html.twig',$render);
                break;
            case 'ponctuel':
                return $this->render('ArteliBundle::realisation_ponctuel.html.twig',$render);
                break;
            case 'actu' :
                return $this->render('ArteliBundle::actu.html.twig',$render);
                break;
//            case 'actuponctuel' :
//                return $this->render('ArteliBundle::actuponctuel.html.twig',$render);
//                break;
            case 'entreprise' :
                return $this->render('ArteliBundle::entreprise.html.twig',$render);
                break;
            case 'realisation_projet_entreprise' :
                return $this->render('ArteliBundle::realisation_projet_entreprise.html.twig',$render);
                break;
            case 'lieu' :
                return $this->render('ArteliBundle::atelier.html.twig',$render);
                break;
            case 'partenaire' :
                return $this->render('ArteliBundle::partenaire.html.twig',$render);
                break;
            case 'localisation' :
                return $this->render('ArteliBundle::localisation.html.twig',$render);
                break;
            case 'installation' :
                return $this->render('ArteliBundle::installation.html.twig',$render);
                break;
            case 'tressage' :
                return $this->render('ArteliBundle::realisation_tressage.html.twig',$render);
                break;
            case 'carnaval' :
                return $this->render('ArteliBundle::realisation_char_carnaval.html.twig',$render);
                break;
            case 'stages':
                $affiches = array_diff(scandir('img/affiche/',SCANDIR_SORT_DESCENDING), array('..', '.'));
                foreach ($affiches as $key => $affiche) {
                    $affiches[$key] = 'img/affiche/'.$affiche;
                }
                $render['affiches'] = $affiches;



                //Formulaire de reservation
                // creates a task object and initializes some data for this example

            $maxReservationPerWed = 4;
            $em = $this->getDoctrine()->getManager();
                $ts  = strtotime("this week tuesday");
//                $ts  = strtotime("next tuesday");
                $end = (date('d') >= 25) ? strtotime("last tuesday of next month") : strtotime("last tuesday of this month");
                $wednesdays = array();
                $availableWed = array();
                $reservations = array();
                $exclusionDates = $em->getRepository(GrrExclude::class)->findAll();
                foreach ($exclusionDates as $exclusionDate) {
                    $exclude[] = $exclusionDate->getExclude()->getTimestamp();

                }

                $tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"));
                while($ts <= $end) {
                    if (!in_array($ts,$exclude)) {
                        $reservation = $em->getRepository(Grr::class)->findBy(['wednesday' => $ts]);
                        if (count($reservation) < $maxReservationPerWed) {
                            //check si on est la veille pour empecher la reservation
                            if ($tomorrow !== $ts && $ts > strtotime("today")) {
                                $availableWed[] = $ts;
                            }
                        }
                        $reservations[] = $reservation;
                        $wednesdays[] = $ts;

                    }
                    $ts = strtotime('next tuesday', $ts);
                }

                $grr = new Grr();
                $form = $this->createFormBuilder($grr)
                    ->add('username', TextType::class,['label' => 'Votre nom : '])
                    ->add('email', TextType::class,['label' => 'Votre email : '])
                    ->add('wednesday', ChoiceType::class,[
                        'label' => 'Réservation pour le Mardi : ',
                        'choices' => $availableWed,
                        'choice_label' => function($wednesdays, $key, $value) {
                            return date('d/m/Y',$value);
                        },


                    ])
                    ->add('save', SubmitType::class, ['label' => 'Réserver'])
                    ->setAction($this->generateUrl('grr_new'))
                    ->getForm();

                $render['formgrr'] = $form->createView();
                $render['reservations'] = $reservations;
                $render['wednesdays'] = $wednesdays;
                $render['maxSeat'] = $maxReservationPerWed;


                return $this->render('ArteliBundle::stages.html.twig',$render);
                break;
            case 'speakingofwhich':
                return $this->render('ArteliBundle::article-press.html.twig',$render);
                break;

            default:
                return $this->render('ArteliBundle::presentation.html.twig',$render);
                break;

        }
    }



 
    /**
     * @Route("/download/{dlfile}",name="arteli_download")
     * @return Response
     */
    public function downloadReglementAction($dlfile){

//        $fichier = "reglement_interieur_atelier_arteli.pdf";
        $chemin = "/../web/img/download/"; // emplacement de votre fichier .pdf
        $path = $this->get('kernel')->getRootDir().$chemin;
        $response = new Response();
        $response->setContent(file_get_contents($this->get('kernel')->getRootDir().$chemin.$dlfile));
        $response->headers->set('Content-Type', 'application/force-download'); // modification du content-type pour forcer le t�l�chargement (sinon le navigateur internet essaie d'afficher le document)
        $response->headers->set('Content-disposition', 'filename='. $dlfile);

        return $response;
    }

}

