<?php

namespace ArteliBundle\Controller;

use ArteliBundle\Entity\Fresque;
use ArteliBundle\Form\FresqueType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use ArteliBundle\Entity\Newsletter;
use ArteliBundle\Form\NewsletterType;
use ArteliBundle\Form\ContactType;

/**
 * Class ContactController
 * @package ArteliBundle\Controller
 * @Route("/contact")
 */
class ContactController extends Controller
{
    /**
     * @Route("/mail",name="arteli_send_mail")
     */
    public function emailAction(Request $request){
        $contactform = $this->createForm(ContactType::class);
        if ($request->isMethod('POST') && $contactform->handleRequest($request)->isValid()) {
            $data = $contactform->getData();
            $message = \Swift_Message::newInstance()
                ->setSubject('[Arteli] Message')
                ->setFrom($data['email'])
                ->setTo('arteli.association@gmail.com')
                ->setBcc('sylvain.ribaillier@gmail.com')
                ->setBody($data['message'],'text/html');
            $this->get('mailer')->send($message);
        }
        return $this->redirectToRoute('arteli_home');
    }

    /**
     * @Route("/newsletter",name="arteli_email_subscribe")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function addemailAction(Request $request){
        $email = new Newsletter();
        $email->setSend(true);
        $form = $this->createForm(NewsletterType::class, $email);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($email);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Email bien enregistr�e.');

            return $this->redirectToRoute('arteli_home');
        }

        return $this->render('@Arteli/Contact/addemail.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    /**
     * @Route("/parleznousdevotrequartier",name="arteli_send_word_fresque")
     */
    public function emailmotfresqueAction(Request $request){
        $mot = new Fresque();
        $fresqueform = $this->createForm(FresqueType::class,$mot);
        if ($request->isMethod('POST') && $fresqueform->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($mot);
            $em->flush();
                $this->get('session')->getFlashBag()->add('info','MERCI POUR VOTRE CONTRIBUTION !');
//            }
        }
        //faire en sorte de ne plus afficher la popup
        return $this->redirectToRoute('arteli_home');
    }

}
