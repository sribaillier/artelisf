<?php

namespace ArteliBundle\Controller;

use ArteliBundle\Entity\Grr;
use Swift_Mailer;
use Swift_Message;
use Swift_SendmailTransport;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Grr controller.
 *
 * @Route("grr")
 */
class GrrController extends Controller
{
    /**
     * Lists all grr entities.
     *
     * @Route("/cancel/{hash}", name="grr_cancel_reservation")
     * @Method("GET")
     */
    public function cancelReservationAction($hash)
    {
        $em = $this->getDoctrine()->getManager();
        $tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"));

        /** @var Grr $grr */
        $grr = $em->getRepository('ArteliBundle:Grr')->findOneBy(['hash' => $hash]);

        //Annulation impossible la veille
        $subject = '[Arteli] Annulation Réservation temps de pratique artistique libre et autonome';
        if ($tomorrow !== $grr->getWednesday()) {
            $em->remove($grr);
            $em->flush();
            $body ="<p>Votre annulation de réservation (".$grr->getWednesdayFormated().") a bien été prise en compte.</p>
                    <p>Cordialement.</p>";
        }
        else {
            $body ="<p>Vous ne pouvez pas annuler votre réservation (".$grr->getWednesdayFormated().") la veille de la date de reservation</p>
                    <p>Merci de prendre contact directement avec Mélanie (06 63 78 48 26)</p>
                    <p>Cordialement.</p>";
        }


        $transport = new Swift_SendmailTransport('/usr/sbin/sendmail -t');

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);

// Create a message
        $message = (new Swift_Message($subject))
            ->setFrom('arteli.association@gmail.com')
            ->setTo($grr->getEmail())
            ->setBcc(['as.masson@hotmail.fr','sylvain.ribaillier@gmail.com'])
            ->setBody($body,'text/html');

        // Send the message
        $mailer->send($message);




        return $this->redirectToRoute('arteli_switch',['switch' =>'stages']);
    }

    /**
     * Creates a new grr entity.
     *
     * @Route("/new", name="grr_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $hash =uniqid();
        $grr = new Grr();
        $grr->setHash($hash);
        $grr->setUsername($_POST['form']['username']);
        $grr->setEmail($_POST['form']['email']);
        $grr->setWednesday($_POST['form']['wednesday']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($grr);
        $em->flush();

        $body = "<p>Bonjour,</p>
                <p>Votre demande de réservation du ".$grr->getWednesdayFormated()." pour l'atelier de pratique libre à Arteli a bien été prise en compte.</p>
                
                <p>Pour annuler cette demande cliquez sur le lien suivant : https://arteli-association.fr/grr/cancel/".$hash."</p>
                
                <p>N'oubliez pas d'apporter votre matériel.</p> 
                
                <p>Bien à vous</p> 
                
                <p>L'équipe d'Arteli</p>";

        $transport = new Swift_SendmailTransport('/usr/sbin/sendmail -t');

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);

// Create a message
        $message = (new Swift_Message('[Arteli] Réservation temps de pratique artistique libre et autonome'))
            ->setFrom('arteli.association@gmail.com')
            ->setTo($_POST['form']['email'])
            ->setBcc(['as.masson@hotmail.fr','sylvain.ribaillier@gmail.com'])
            ->setBody($body,'text/html');

        // Send the message
        $mailer->send($message);


        return $this->redirectToRoute('arteli_switch',['switch' =>'stages']);
    }

    /**
     * Finds and displays a grr entity.
     *
     * @Route("/{id}", name="grr_show")
     * @Method("GET")
     */
    public function showAction(Grr $grr)

    {
        $deleteForm = $this->createDeleteForm($grr);

        return $this->render('grr/show.html.twig', array(
            'grr' => $grr,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing grr entity.
     *
     * @Route("/{id}/edit", name="grr_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Grr $grr)
    {
        $deleteForm = $this->createDeleteForm($grr);
        $editForm = $this->createForm('ArteliBundle\Form\GrrType', $grr);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('grr_edit', array('id' => $grr->getId()));
        }

        return $this->render('grr/edit.html.twig', array(
            'grr' => $grr,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a grr entity.
     *
     * @Route("/{id}", name="grr_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Grr $grr)
    {
        $form = $this->createDeleteForm($grr);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($grr);
            $em->flush();
        }

        return $this->redirectToRoute('grr_index');
    }

    /**
     * Creates a form to delete a grr entity.
     *
     * @param Grr $grr The grr entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Grr $grr)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grr_delete', array('id' => $grr->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
