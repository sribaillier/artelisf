<?php

namespace ArteliBundle\Controller;

use ArteliBundle\Entity\GrrExclude;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Grrexclude controller.
 *
 * @Route("admin/grrexclude")
 */
class GrrExcludeController extends Controller
{
    /**
     * Lists all grrExclude entities.
     *
     * @Route("/", name="admin_grrexclude_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $grrExcludes = $em->getRepository('ArteliBundle:GrrExclude')->findAll();

        return $this->render('grrexclude/index.html.twig', array(
            'grrExcludes' => $grrExcludes,
        ));
    }

    /**
     * Creates a new grrExclude entity.
     *
     * @Route("/new", name="admin_grrexclude_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $grrExclude = new Grrexclude();
        $form = $this->createForm('ArteliBundle\Form\GrrExcludeType', $grrExclude);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($grrExclude);
            $em->flush();

            return $this->redirectToRoute('admin_grrexclude_show', array('id' => $grrExclude->getId()));
        }

        return $this->render('grrexclude/new.html.twig', array(
            'grrExclude' => $grrExclude,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a grrExclude entity.
     *
     * @Route("/{id}", name="admin_grrexclude_show")
     * @Method("GET")
     */
    public function showAction(GrrExclude $grrExclude)
    {
        $deleteForm = $this->createDeleteForm($grrExclude);

        return $this->render('grrexclude/show.html.twig', array(
            'grrExclude' => $grrExclude,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing grrExclude entity.
     *
     * @Route("/{id}/edit", name="admin_grrexclude_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, GrrExclude $grrExclude)
    {
        $deleteForm = $this->createDeleteForm($grrExclude);
        $editForm = $this->createForm('ArteliBundle\Form\GrrExcludeType', $grrExclude);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_grrexclude_edit', array('id' => $grrExclude->getId()));
        }

        return $this->render('grrexclude/edit.html.twig', array(
            'grrExclude' => $grrExclude,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a grrExclude entity.
     *
     * @Route("/{id}", name="admin_grrexclude_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, GrrExclude $grrExclude)
    {
        $form = $this->createDeleteForm($grrExclude);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($grrExclude);
            $em->flush();
        }

        return $this->redirectToRoute('admin_grrexclude_index');
    }

    /**
     * Creates a form to delete a grrExclude entity.
     *
     * @param GrrExclude $grrExclude The grrExclude entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(GrrExclude $grrExclude)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_grrexclude_delete', array('id' => $grrExclude->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
