<?php

namespace ArteliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fresque
 *
 * @ORM\Table(name="fresque")
 * @ORM\Entity(repositoryClass="ArteliBundle\Repository\FresqueRepository")
 */
class Fresque
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mot", type="string", length=255)
     */
    private $mot;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mot
     *
     * @param string $mot
     *
     * @return Fresque
     */
    public function setMot($mot)
    {
        $this->mot = $mot;

        return $this;
    }

    /**
     * Get mot
     *
     * @return string
     */
    public function getMot()
    {
        return $this->mot;
    }
}

