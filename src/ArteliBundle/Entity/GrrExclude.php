<?php

namespace ArteliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GrrExclude
 *
 * @ORM\Table(name="grr_exclude")
 * @ORM\Entity(repositoryClass="ArteliBundle\Repository\GrrExcludeRepository")
 */
class GrrExclude
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="exclude", type="datetime")
     */
    private $exclude;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set exclude
     *
     * @param \DateTime $exclude
     *
     * @return GrrExclude
     */
    public function setExclude($exclude)
    {
        $this->exclude = $exclude;

        return $this;
    }

    /**
     * Get exclude
     *
     * @return \DateTime
     */
    public function getExclude()
    {
        return $this->exclude;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return GrrExclude
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
