<?php

namespace ArteliBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class FresqueType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mot',TextType::class,array('attr'=>array('placeholder'=>'Votre quartier en un mot')))
            ->add('save',SubmitType::class,array('label' => 'Envoyer'))
        ;
    }
    
}
